package net.omega.master.server.gameserver;

import io.netty.util.AttributeKey;

/**
 *
 * @author Kyu
 */
public class GameServerAttachment {

    public static final AttributeKey<GameServerAttachment> ATTRIB_KEY = new AttributeKey<GameServerAttachment>("server.attachment");
    private GameServer gameServer;

    public GameServerAttachment(GameServer gameServer) {
        this.gameServer = gameServer;
    }

    public GameServer getGameServer() {
        return gameServer;
    }

}
