package net.omega.master.server.gameserver;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import net.omega.master.server.database.GameServerQuery;

/**
 *
 * @author Kyu
 */
public class GameServerManager {

    private static final Logger logger = Logger.getLogger(GameServerManager.class.getName());
    private Map<Integer, GameServer> gameservers = new HashMap<Integer, GameServer>();
    private static GameServerManager instance;

    public GameServerManager() {
    }

    public static GameServerManager getInstance() {
        if (instance == null)
            instance = new GameServerManager();

        return instance;
    }

    public void addGameServer(GameServer gameServer) {
        GameServerQuery.getInstance().insertGameServer(gameServer);
        gameservers.put(gameServer.getId(), gameServer);
    }

    public void removeGameServer(GameServer gameServer) {
        removeGameServer(gameServer.getId());
    }

    public void removeGameServer(int id) {
        GameServerQuery.getInstance().deleteGameServer(id);
        gameservers.remove(id);
    }

    public GameServer getGameServer(int id) {
        return gameservers.get(id);
    }
}
