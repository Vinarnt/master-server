package net.omega.master.server.gameserver;

/**
 *
 * @author Kyu
 */
public class GameServer {

    
    
    private int id;
    private String name;
    private String host;
    private int port;
    private boolean hasPassword;
    private boolean isDedicated;
    private int currentPlayers;
    private int maxPlayers;
    private String gameMode;
    private String map;

    public GameServer(int id, String name, String host, int port, boolean hasPassword, boolean isDedicated, int currentPlayers, int maxPlayers, String gameMode, String map) {
        this.id = id;
        this.name = name;
        this.host = host;
        this.port = port;
        this.hasPassword = hasPassword;
        this.isDedicated = isDedicated;
        this.currentPlayers = currentPlayers;
        this.maxPlayers = maxPlayers;
        this.gameMode = gameMode;
        this.map = map;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean hasPassword() {
        return hasPassword;
    }

    public void setHasPassword(boolean hasPassword) {
        this.hasPassword = hasPassword;
    }

    public boolean isDedicated() {
        return isDedicated;
    }

    public void setIsDedicated(boolean isDedicated) {
        this.isDedicated = isDedicated;
    }

    public int getCurrentPlayers() {
        return currentPlayers;
    }

    public void setCurrentPlayers(int currentPlayers) {
        this.currentPlayers = currentPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getGameMode() {
        return gameMode;
    }

    public void setGameMode(String gameMode) {
        this.gameMode = gameMode;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }
}