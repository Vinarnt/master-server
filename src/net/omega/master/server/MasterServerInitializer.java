package net.omega.master.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import net.omega.master.server.codec.PacketDecoder;
import net.omega.master.server.codec.PacketEncoder;

/**
 *
 * @author Kyu
 */
public class MasterServerInitializer extends ChannelInitializer<SocketChannel> {

    public MasterServerInitializer() {
    }

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline _pipeline = channel.pipeline();
        
        _pipeline.addLast("logHandler", new LoggingHandler(LogLevel.INFO));
        _pipeline.addLast("decoder", new PacketDecoder());
        _pipeline.addLast("encoder", new PacketEncoder());
        _pipeline.addLast("handler", new MasterServerHandler());
    }
    
}
