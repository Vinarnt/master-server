package net.omega.master.server.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import java.util.List;
import net.omega.master.server.packet.RegisterPacket;
import net.omega.master.server.packet.ServerListPacket;
import net.omega.master.server.packet.ServerUpdatePlayerPacket;

/**
 *
 * @author Kyu
 */
public class PacketDecoder extends MessageToMessageDecoder<ByteBuf> {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        ByteBufInputStream _buffer = new ByteBufInputStream(in);
        byte _packetID = _buffer.readByte();
        switch(_packetID)
        {
            case ServerListPacket.PACKET_ID:
                ServerListPacket _slPacket = new ServerListPacket();
                _slPacket.decode(_buffer, ctx.channel());
                out.add(_slPacket);
                
                break;
                
            case RegisterPacket.PACKET_ID:
                RegisterPacket _registerPacket = new RegisterPacket();
                _registerPacket.decode(_buffer, ctx.channel());
                out.add(_registerPacket);
                
                break;
                
            case ServerUpdatePlayerPacket.PACKET_ID:
                ServerUpdatePlayerPacket _updatePacket = new ServerUpdatePlayerPacket();
                _updatePacket.decode(_buffer, ctx.channel());
                out.add(_updatePacket);
                
                break;
        }
        _buffer.close();
    }
    
}
