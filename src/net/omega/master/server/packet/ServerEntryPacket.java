package net.omega.master.server.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class ServerEntryPacket extends AbstractPacket {

    private static final Logger logger = Logger.getLogger(ServerEntryPacket.class.getName());
    public static final byte PACKET_ID = 0x2;
    private String name;
    private String host;
    private int port;
    private boolean isDedicated;
    private boolean isPassword;
    private int currentPlayers;
    private int maxPlayers;
    private String gameMode;
    private String map;

    @Override
    public void decode(ByteBufInputStream buffer, Channel channel) {
    }

    @Override
    public void execute(Channel channel) {
    }

    @Override
    public void encode(Channel channel) {
        try {
            out = new ByteBufOutputStream(Unpooled.buffer(1024));
            out.writeByte(PACKET_ID);
            out.writeUTF(name);
            out.writeUTF(host);
            out.writeInt(port);
            out.writeBoolean(isDedicated);
            out.writeBoolean(isPassword);
            out.writeInt(currentPlayers);
            out.writeInt(maxPlayers);
            out.writeUTF(gameMode);
            out.writeUTF(map);
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Unable to encode packet", ex);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setIsDedicated(boolean isDedicated) {
        this.isDedicated = isDedicated;
    }

    public void setIsPassword(boolean isPassword) {
        this.isPassword = isPassword;
    }

    public void setCurrentPlayers(int currentPlayers) {
        this.currentPlayers = currentPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public void setGameMode(String gameMode) {
        this.gameMode = gameMode;
    }

    public void setMap(String map) {
        this.map = map;
    }
}
