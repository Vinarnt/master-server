package net.omega.master.server.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.Channel;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.master.server.database.DatabaseHandler;
import net.omega.master.server.database.PreparedStatementParameterTask;

/**
 * SERVERLIST 0x1 filters : - name string - isPassword boolean - isFull boolean
 * - isEmpty boolean - isDedicated boolean - gamemode short - map short
 *
 * @author Kyu
 */
public class ServerListPacket extends AbstractPacket {

    private static final Logger logger = Logger.getLogger(ServerListPacket.class.getName());
    public static final byte PACKET_ID = 0x1;
    // filters
    private String name;
    private boolean isPassword;
    private boolean isFull;
    private boolean isEmpty;
    private boolean isDedicated;
    private String gameMode;
    private String map;

    public ServerListPacket() {
    }

    @Override
    public void decode(ByteBufInputStream buffer, Channel channel) {
        try {
            name = buffer.readUTF();
            isPassword = buffer.readBoolean();
            isFull = buffer.readBoolean();
            isEmpty = buffer.readBoolean();
            isDedicated = buffer.readBoolean();
            gameMode = buffer.readUTF();
            map = buffer.readUTF();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Unable to build packet", ex);
        }
    }

    @Override
    public void execute(Channel channel) {
        try {
            final LinkedList<String> _whereClauses = new LinkedList<String>();
            final LinkedList<PreparedStatementParameterTask> _params = new LinkedList<PreparedStatementParameterTask>();
            final StringBuilder _queryBuilder = new StringBuilder();
            _queryBuilder.append("SELECT * ");
            _queryBuilder.append("FROM servers ");

            if (!name.isEmpty()) {
                _whereClauses.add("Name LIKE ?");
                _params.add(new PreparedStatementParameterTask() {
                    @Override
                    public void setParam(PreparedStatement prepStat, int paramNum) throws SQLException {
                        prepStat.setString(paramNum, "%" + name + "%");
                    }
                });
            }
            if (!isPassword) {
                _whereClauses.add("HasPassword == 0");
            }
            if (!isFull) {
                _whereClauses.add("CurrentPlayers < MaxPlayers");
            }
            if (!isEmpty) {
                _whereClauses.add("CurrentPlayers > 0");
            }
            if (isDedicated) {
                _whereClauses.add("IsDedicated == 1");
            }
            if (!gameMode.isEmpty()) {
                _whereClauses.add("GameMode LIKE ?");
                _params.add(new PreparedStatementParameterTask() {

                    @Override
                    public void setParam(PreparedStatement prepStat, int paramNum) throws SQLException {
                        prepStat.setString(paramNum, "%" + gameMode + "%");
                    }
                });
            }
            if (!map.isEmpty()) {
                _whereClauses.add("Map LIKE ?");
                _params.add(new  PreparedStatementParameterTask() {

                    @Override
                    public void setParam(PreparedStatement prepStat, int paramNum) throws SQLException {
                        prepStat.setString(paramNum, "%" + map + "%");
                    }
                });
            }
            if (!_whereClauses.isEmpty()) {
                _queryBuilder.append("WHERE ");
                _queryBuilder.append(_whereClauses.pop());
                while (!_whereClauses.isEmpty()) {
                    _queryBuilder.append(" AND ");
                    _queryBuilder.append(_whereClauses.pop());
                }
            }
            logger.log(Level.INFO, _queryBuilder.toString());
            PreparedStatement _prepStat = DatabaseHandler.getInstance().getConnection().prepareStatement(_queryBuilder.toString());
            int i = 0;
            while (!_params.isEmpty()) {
                i++;
                _params.pop().setParam(_prepStat, i);
                
            }
            final ResultSet _result = _prepStat.executeQuery();
            while (_result.next()) {
                ServerEntryPacket _serverEntry = new ServerEntryPacket();
                _serverEntry.setName(_result.getString("Name"));
                _serverEntry.setHost(_result.getString("Host"));
                _serverEntry.setPort(_result.getInt("Port"));
                _serverEntry.setIsDedicated(_result.getBoolean("IsDedicated"));
                _serverEntry.setIsPassword(_result.getBoolean("HasPassword"));
                _serverEntry.setGameMode(_result.getString("GameMode"));
                _serverEntry.setMap(_result.getString("Map"));
                _serverEntry.setCurrentPlayers(_result.getInt("CurrentPlayers"));
                _serverEntry.setMaxPlayers(_result.getInt("MaxPlayers"));
                
                channel.writeAndFlush(_serverEntry);
            }
            _result.close();
            _prepStat.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Unable to register server correctly", ex);
        } finally {
            channel.disconnect();
        }
    }

    @Override
    public void encode(Channel channel) {
    }
}
