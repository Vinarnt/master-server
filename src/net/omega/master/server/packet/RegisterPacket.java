package net.omega.master.server.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.Channel;
import io.netty.util.Attribute;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.master.server.gameserver.GameServer;
import net.omega.master.server.gameserver.GameServerAttachment;
import net.omega.master.server.gameserver.GameServerManager;

/**
 *
 * @author Kyu
 */
public class RegisterPacket extends AbstractPacket {

    private static final Logger logger = Logger.getLogger(RegisterPacket.class.getName());
    public static final byte PACKET_ID = 0x3;
    private String serverName;
    private int port;
    private boolean isDedicated;
    private boolean hasPassword;
    private int maxPlayers;
    private String gamemode;
    private String map;

    @Override
    public void decode(ByteBufInputStream buffer, Channel channel) {
        try {
            serverName = buffer.readUTF();
            port = buffer.readInt();
            isDedicated = buffer.readBoolean();
            hasPassword = buffer.readBoolean();
            maxPlayers = buffer.readInt();
            gamemode = buffer.readUTF();
            map = buffer.readUTF();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Unable to decode packet", ex);
        }
    }

    @Override
    public void execute(Channel channel) {
        logger.log(Level.INFO, "[REGISTER] {0}", serverName);

        String _hostName = ((InetSocketAddress) channel.remoteAddress()).getHostName();
        GameServer _gs = new GameServer(-1, serverName, _hostName, port, hasPassword, isDedicated, 0, maxPlayers, gamemode, map);
        Attribute<GameServerAttachment> _attr = channel.attr(GameServerAttachment.ATTRIB_KEY);
        _attr.set(new GameServerAttachment(_gs));
        GameServerManager.getInstance().addGameServer(_gs);
    }

    @Override
    public void encode(Channel channel) {
    }
}
