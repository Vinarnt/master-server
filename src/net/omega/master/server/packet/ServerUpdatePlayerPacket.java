package net.omega.master.server.packet;

import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.master.server.database.DatabaseHandler;
import net.omega.master.server.database.GameServerQuery;
import net.omega.master.server.gameserver.GameServer;
import net.omega.master.server.gameserver.GameServerAttachment;
import net.omega.master.server.gameserver.GameServerManager;

/**
 *
 * @author Kyu
 */
public class ServerUpdatePlayerPacket extends AbstractPacket {
    
    private static final Logger logger = Logger.getLogger(ServerUpdatePlayerPacket.class.getName());
    public static final byte PACKET_ID = 0x4;
    private int currentPlayers;
    private int maxPlayers;
    
    public ServerUpdatePlayerPacket() {
    }
    
    @Override
    public void decode(ByteBufInputStream buffer, Channel channel) {
        try {
            currentPlayers = buffer.readInt();
            maxPlayers = buffer.readInt();
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Unable to build packet", ex);
        }
    }
    
    @Override
    public void execute(Channel channel) {
        GameServer _gs = GameServerManager.getInstance().getGameServer(channel.attr(GameServerAttachment.ATTRIB_KEY).get().getGameServer().getId());
        _gs.setCurrentPlayers(currentPlayers);
        GameServerQuery.getInstance().updateGameServer(_gs);
    }
    
    @Override
    public void encode(Channel channel) {
        out = new ByteBufOutputStream(Unpooled.buffer(64));
        try {
            out.writeByte(PACKET_ID);
            out.writeInt(currentPlayers);
            out.writeInt(maxPlayers);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Unable to build packet", ex);
        }
    }
}
