package net.omega.master.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.master.Constants;

/**
 *
 * @author Kyu
 */
public class MasterServerFactory {

    private static final Logger logger = Logger.getLogger(MasterServerFactory.class.getName());
    private static MasterServerFactory instance;

    public MasterServerFactory() {
    }

    public static MasterServerFactory getInstance() {
        if (instance == null)
            instance = new MasterServerFactory();

        return instance;
    }

    public void start() {
        EventLoopGroup _bossGroup = new NioEventLoopGroup();
        EventLoopGroup _workerGroup = new NioEventLoopGroup();
        
        try {
            new ServerBootstrap()
                    .group(_bossGroup, _workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new MasterServerInitializer())
                    .bind(Constants.SERVER_PORT)
                    .sync()
                    .channel()
                    .closeFuture()
                    .sync();
            logger.log(Level.INFO, "Server started");
        } catch (InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            _bossGroup.shutdownGracefully();
            _workerGroup.shutdownGracefully();
        }
    }
}
