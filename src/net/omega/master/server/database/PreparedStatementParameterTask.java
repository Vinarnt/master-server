package net.omega.master.server.database;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Kyu
 */
public interface PreparedStatementParameterTask {
    
    public void setParam(PreparedStatement prepStat, int paramNum) throws SQLException;
    
}
