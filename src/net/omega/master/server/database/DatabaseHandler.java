package net.omega.master.server.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class DatabaseHandler {

    private static final Logger logger = Logger.getLogger(DatabaseHandler.class.getName());
    private Connection connection;
    private static DatabaseHandler instance;

    public DatabaseHandler() {
    }

    public static DatabaseHandler getInstance() {
        if (instance == null)
            instance = new DatabaseHandler();

        return instance;
    }

    public void initialize() {
        logger.log(Level.INFO, "Initialize sqlite database connection");
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE, "Unable to load sqlite jdbc driver", ex);
            System.exit(-1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:servers.db");
            Statement _statement = connection.createStatement();
            _statement.setQueryTimeout(10);

            _statement.executeUpdate("DROP TABLE IF EXISTS servers");
            _statement.executeUpdate("CREATE TABLE servers ("
                    + "ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "Name STRING, "
                    + "Host STRING, "
                    + "Port INTEGER, "
                    + "HasPassword BIT, "
                    + "IsDedicated BIT, "
                    + "CurrentPlayers INTEGER, "
                    + "MaxPlayers INTEGER, "
                    + "GameMode STRING, "
                    + "Map STRING)");
            _statement.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Unable to open connection to the database", ex);
            System.exit(-1);
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void close() {
        logger.log(Level.INFO, "CLose sqlite database connection");
        try {
            connection.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Cannot close the sqlite database connection", ex);
        }
    }
}
