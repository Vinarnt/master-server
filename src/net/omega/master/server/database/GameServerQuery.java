package net.omega.master.server.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.master.server.gameserver.GameServer;

/**
 *
 * @author Kyu
 */
public class GameServerQuery {

    private static final Logger logger = Logger.getLogger(GameServerQuery.class.getName());
    private static GameServerQuery instance;

    public GameServerQuery() {
    }

    public static GameServerQuery getInstance() {
        if (instance == null)
            instance = new GameServerQuery();

        return instance;
    }

    public void insertGameServer(GameServer gameServer) {
        try {
            PreparedStatement _prepStat = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "INSERT INTO servers (Name, Host, Port, HasPassword, IsDedicated, CurrentPlayers, MaxPlayers, GameMode, Map) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)");
            _prepStat.setString(1, gameServer.getName());
            _prepStat.setString(2, gameServer.getHost());
            _prepStat.setInt(3, gameServer.getPort());
            _prepStat.setBoolean(4, gameServer.hasPassword());
            _prepStat.setBoolean(5, gameServer.isDedicated());
            _prepStat.setInt(6, gameServer.getCurrentPlayers());
            _prepStat.setInt(7, gameServer.getMaxPlayers());
            _prepStat.setString(8, gameServer.getGameMode());
            _prepStat.setString(9, gameServer.getMap());
            _prepStat.executeUpdate();
            ResultSet _result = _prepStat.getGeneratedKeys();
            if (_result.next()) {
                int _autoIncr = _result.getInt(1);
                gameServer.setId(_autoIncr);
            } else {
                logger.severe("Unable to get generated id");
            }
            _result.close();
            _prepStat.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Unable to save server in database", ex);
        }
    }
    
    public void updateGameServer(GameServer gameServer) {
        try {
            PreparedStatement _prepStat = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "UPDATE servers SET Name = ?, HasPassword = ?, CurrentPlayers = ?, MaxPlayers = ?, GameMode = ?, Map = ? WHERE ID = ?");
            _prepStat.setString(1, gameServer.getName());
            _prepStat.setBoolean(2, gameServer.hasPassword());
            _prepStat.setInt(3, gameServer.getCurrentPlayers());
            _prepStat.setInt(4, gameServer.getMaxPlayers());
            _prepStat.setString(5, gameServer.getGameMode());
            _prepStat.setString(6, gameServer.getMap());
            _prepStat.setInt(7, gameServer.getId());
            _prepStat.executeUpdate();
            _prepStat.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Unable to update server in database", ex);
        }
    }
    
    public void deleteGameServer(int id) {
        try {
            PreparedStatement _prepStat = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "DELETE FROM servers WHERE ID = ?");
            _prepStat.setInt(1, id);
            _prepStat.executeUpdate();
            _prepStat.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Unable to delete server from database", ex);
        }
    }
}
