package net.omega.master.server;

import net.omega.master.server.gameserver.GameServerAttachment;
import net.omega.master.server.database.DatabaseHandler;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.omega.master.server.packet.AbstractPacket;

/**
 *
 * @author Kyu
 */
public class MasterServerHandler extends SimpleChannelInboundHandler<AbstractPacket> {

    private static final Logger logger = Logger.getLogger(MasterServerHandler.class.getName());

    public MasterServerHandler() {
    }

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        logger.log(Level.INFO, "New connection from {0}", ctx.channel().remoteAddress());
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        logger.log(Level.INFO, "Lost connection from {0}", ctx.channel().remoteAddress());
        Channel _channel = ctx.channel();
        if (_channel.attr(GameServerAttachment.ATTRIB_KEY).get() != null) {
            int _id = _channel.attr(GameServerAttachment.ATTRIB_KEY).get().getGameServer().getId();
            PreparedStatement _prepStat = DatabaseHandler.getInstance().getConnection().prepareStatement("DELETE FROM servers WHERE ID = ?");
            _prepStat.setInt(1, _id);
            _prepStat.executeUpdate();
            _prepStat.close();
        }
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, AbstractPacket packet) throws Exception {
        Channel _channel = ctx.channel();
        packet.execute(_channel);
    }
}
