package net.omega.master;

import net.omega.master.server.database.DatabaseHandler;
import net.omega.master.server.MasterServerFactory;

public class Main {

    public static void main(String[] args) {
        DatabaseHandler.getInstance().initialize();
        MasterServerFactory.getInstance().start();
    }
    
}
